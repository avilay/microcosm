## Build the Docker container
```
docker build -t frontend .
```

## Run app locally
```
docker run -p 8080:80 --name frontend-app frontend
```

To run it as a daemon
```
docker run -d -p 8080:80 --name frontend-app frontend
```

## Push Docker image to gcr
[Doc](https://cloud.google.com/container-registry/docs/pushing-and-pulling)

Configure the `docker` utility to work with GCR creds.
```
gcloud auth configure-docker
```

Rename the frontend image created above with the FQDN of the image's desired location.
```
docker tag frontend gcr.io/unity-ml-avilay-test/frontend
```

Then push the image to GCR
```
docker push gcr.io/unity-ml-avilay-test/frontend
```

## Deploy to GKE
Create the namespace first
```
kubectl -f deploy/namespace.yaml
```

Create the deployment. This will create 2 replicas on two different pods.
```
kubectl -f deploy/deployment.yaml
```

## Test on GKE
So far I have deployed a Deployment to GKE. This in and of itself is supposed to be available outside of the K8 cluster. The way to make a Deployment available to the outside world is using a Service, which is described later. For the time being, I'll take advantage of the fact that the GKE cluster has been deployed in the same network as my jumpbox, so any internal pod IP address will be reachable from my jumpbox.

Find out the pod names
```
kubectl get pods -n aptest
```

I can also get the pod names of pods with the label `run=frontend`.
```
kubectl -n aptest get pods -l run=frontend
```

Get details for one of the frontend pod
```
kubectl describe pods -n aptest <podname>
```

The details will have the 10.* IP address of the pod.

Log into the jumpbox in GCP, which is in the same network as the GKE cluster.
```
gcloud compute ssh def-jumpbox
```

From the jumpbox make a couple of requests to the deployment
```
curl http://10.8.77.75/wirecheck
```

Create a file called frontend_payload.json with the following contents:
```
{
    "id": 1,
    "consistent": true,
    "rating": 3.4,
    "quote": "hello world",
    "activities": [
        {
            "timestamp": "2018-01-01T00:00:00",
            "name": "Tennis"
        },
        {
            "timestamp": "2018-01-02T00:00:00",
            "name": "Swimming"
        }
    ]
}
```

Run a POST request on the deployment
```
curl -d "@frontend_payload.json" -H "Content-Type: application/json" POST http://10.8.77.75/fast
```

There is no load balancing happening here, I have to make a request directly to each replica pod's IP address to route the request there. There is no single IP address for this service that I can hit.

## Tail logs
This can be done on my local machine, don't have to do this from the jumpbox.
```
kubectl -n aptest logs -f <podname>
```

## Launching an internal Service
A service allows all the pods in the service to be addressable by a single IP address. There is always a service called `kubernetes` running in the `default` namespace.
```
kubectl get services
```

But there will be no services running in the `aptest` namespace.
```
kubectl -n aptest get services
```

Launch the ClusterIP service. Such a service will create a unified IP address for all the pods in the deployment, however, this IP will *only* work inside the GKE cluster. It will not work from my `def-jumpbox` even though it is on the same network, because it is not in the GKE cluster.

```
kubectl apply -f deploy/cluster-ip-service.yaml
```

Get the IP address of the service
```
kubectl -n aptest get services
```

If I get the detailed info of this service, I'll see that K8 automatically created another resource of type Endpoint and the IP addresses listed here are those of my deployment pods. K8 constantly monitors for any resources showing up with the selector labels and will add them to the Endpoints (and correspondibly delete the ones that are no longer available).

Try to get to the service from the `def-jumpbox`. It will time out.

## Launch a K8 jumpbox
```
kubectl run -i -t ubuntu-bionic --image=ubuntu --restart=Never
```
This should launch me in the SSH session on the container. Install `curl` if needed. Now if I hit the service IP

## Service request from K8 jumpbox
```
curl http://<service-ip>/wirecheck
```
I should see the `hello world` response. If I am tailing the logs from both the replica pods, I'll see that the request hit one of the replicas. A couple of more requests should result in logs from the other replica as well. The requests don't seem to be routed in a round-robin fashion.

Once I quit the SSH session, the pod will be shutdown.

## Endpoints
As mentioned above, Endpoints are like the LB in-service-dips. K8 is constantly monitoring for the appearance or disappearance of pods with the specified label selector. To see this in action let me try to increase the replicas of the deployment without touching the service. My expectation is that the Endpoints will automatically have the new replica show up.

Bump up the number of replicas in deployment.yaml to 3 and re-deploy it.
```
kubectl apply -f deploy/deployment.yaml
```

Get the ip address of the new pod.
```
kubectl -n aptest get pods -l run=frontend | grep IP
```

Check if the frontend-service Endpoint has this new IP
```
kubectl -n aptest get endpoints frontend-service
```

## Launching an external load balanced service
First shutdown the ClusterIP service to avoid confusion.
```
kubectl -n aptest delete service frontend-service
```

Then deploy the load balanced service.
```
kubectl apply -f deploy/lb-service.yaml
```

After a while, the LB service will have an external IP that I can hit from my laptop.



