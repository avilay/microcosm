docker build -t frontend:$1 .
docker tag frontend:$1 gcr.io/unity-ml-avilay-test/frontend:$1
docker push gcr.io/unity-ml-avilay-test/frontend:$1
echo "Remember to change deployment.yaml to deploy $1"
kubectl apply -f deploy/deployment.yaml
