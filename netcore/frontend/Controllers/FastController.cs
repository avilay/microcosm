using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Frontend.Controllers {
    [Route("/[controller]")]
    [ApiController]
    public class FastController : ControllerBase {
        private readonly ILogger<FastController> _logger;

        public FastController(ILogger<FastController> logger) => _logger = logger ?? throw new ArgumentNullException(nameof(logger));

        [HttpPost]
        public ActionResult<Answer> Post([FromBody] Query query) {
            _logger.LogInformation($"Post {query}");

            var random = new Random();
            var statuses = new Dictionary<HttpStatusCode, double>() {
                {HttpStatusCode.OK, 0.4},
                {HttpStatusCode.Created, 0.2},
                {HttpStatusCode.Accepted, 0.2},

                {HttpStatusCode.NoContent, 0.03},
                {HttpStatusCode.BadRequest, 0.02},
                {HttpStatusCode.Unauthorized, 0.02},
                {HttpStatusCode.Forbidden, 0.01},
                {HttpStatusCode.NotFound, 0.01},
                {HttpStatusCode.MethodNotAllowed, 0.01},

                {HttpStatusCode.InternalServerError, 0.1}
            };
            var status = random.Choose(statuses);
            if (status == HttpStatusCode.OK) {
                var activities = new Activity[] {
                    new Activity{Timestamp=DateTime.Parse("2018-01-01"), Name="Tennis"},
                    new Activity{Timestamp=DateTime.Parse("2018-01-02"), Name="Swimming"},
                };
                return new Answer {
                    Id = 1,
                    Consistent = true,
                    Rating = 3.4,
                    Quote = "hello world",
                    Activities = activities
                };
            } else if (status == HttpStatusCode.Created || status == HttpStatusCode.Accepted) {
                // _logger.LogInformation("{\"severity\": \"INFO\", \"message\": \"Returning a non-200 success\", \"code\": 6754}");
                _logger.LogInformation("{severity} {message} {code}", "INFO", "Returning a non-200 success", 1234);
                return StatusCode((int)status);
            } else if (status == HttpStatusCode.InternalServerError) {
                // _logger.LogError("{\"severity\": \"ERROR\", \"message\": \"Something went horribly wrong!\", \"code\": 1234}");
                _logger.LogError("{severity} {message} {code}", "ERROR", "Something went horribly wrong!", 1234);
                return StatusCode((int)status);
            } else {
                // _logger.LogWarning("{\"severity\": \"WARN\", \"message\": \"Client Error!\", \"code\": 5678}");
                _logger.LogWarning("{severity} {message} {code}", "WARN", "Client error!", 5678);
                return StatusCode((int)status);
            }
        }

        [HttpGet]
        public ActionResult<string> Get() {
            return "hello";
        }
    }
}