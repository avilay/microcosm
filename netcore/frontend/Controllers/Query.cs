using System;

namespace Frontend.Controllers {

    public class DateRange {
        public DateTime From {get; set;}
        public DateTime To {get; set;}

        public override string ToString() {
            return $"<DateRange(From={From} To={To})>";
        }
    }

    public class Query {
        public string Search {get; set;}
        public DateRange Range {get; set;}

        public override string ToString() {
            return $"<Query(Search={Search} Range={Range})>";
        }
    }
}
