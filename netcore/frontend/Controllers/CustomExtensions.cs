using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Frontend.Controllers {
    public static class CustomExtensions {
        public static string Stringify<K, V>(this IDictionary<K, V> dict) {
            var sb = new StringBuilder("{");
            foreach (var kv in dict) {
                sb.AppendFormat($"{kv.Key}=>{kv.Value} ");
            }
            sb.Remove(sb.Length - 1, 1);  // Remove the trailing space
            sb.Append("}");
            return sb.ToString();
        }

        public static string Stringify<T>(this ICollection<T> seq) {
            var sb = new StringBuilder("[");
            foreach (var x in seq) {
                sb.Append(x);
                sb.Append(", ");
            }
            sb.Remove(sb.Length - 1, 1);
            sb.Append("]");
            return sb.ToString();
        }

        private static (IList<T>, IList<double>) CalculateCumProbs<T>(IDictionary<T, double> valProbs) {
            var vals = new List<T>();
            var probs = new List<double>();
            var lastProb = 0.0;
            foreach (var valProb in valProbs) {
                var val = valProb.Key;
                var prob = valProb.Value;
                var currProb = lastProb + prob;
                probs.Add(currProb);
                vals.Add(val);
                lastProb = currProb;
            }
            if (Math.Abs(lastProb - 1.0) > 0.00001) {
                throw new ArgumentException("Probabilities must sum to 1 not {lastProb:0.00000}!");
            }
            return (vals, probs);
        }

        public static T Choose<T>(this Random random, IDictionary<T, double> valProbs) {
            var (vals, probs) = CalculateCumProbs(valProbs);
            var chooseProb = random.NextDouble();
            T choice = default(T);
            for (var i = 0; i < probs.Count; i++) {
                if (chooseProb < probs[i]) {
                    choice = vals[i];
                    break;
                }
            }
            return choice;
        }

        public static IList<T> Choose<T>(this Random random, IDictionary<T, double> valProbs, int size) {
            var (vals, probs) = CalculateCumProbs(valProbs);
            var choices = new List<T>();
            for (var i = 0; i < size; i++) {
                var chooseProb = random.NextDouble();
                for (var j = 0; j < probs.Count; j++) {
                    if (chooseProb < probs[j]) {
                        choices.Add(vals[j]);
                        break;
                    }
                }
            }
            return choices;
        }
    }
}