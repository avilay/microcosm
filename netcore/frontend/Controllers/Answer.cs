using System;

namespace Frontend.Controllers {
    public class Activity {
        public DateTime Timestamp {get; set;}
        public string Name {get; set;}

        public override string ToString() {
            return $"{Timestamp}: {Name}";
        }
    }

    public class Answer {
        public int Id {get; set;}
        public bool Consistent {get; set;}
        public double Rating {get; set;}
        public string Quote {get; set;}
        public Activity[] Activities {get; set;}

        public override string ToString() {
            return $"Answer(Id: {Id}, Consistent: {Consistent}, Rating: {Rating}, Quote: {Quote}, Activities: {Activities})";
        }
    }
}