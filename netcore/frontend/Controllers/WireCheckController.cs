using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WireCheckControllers {
    // [Route("/[controller]")]
    [ApiController]
    public class WireCheckController : ControllerBase {
        private readonly ILogger<WireCheckController> _logger;

        public WireCheckController(ILogger<WireCheckController> logger) {
            _logger = logger;
        }

        [Route("/[controller]/plain")]
        [HttpGet]
        public ActionResult<string> PlainLogs(bool throwException=false) {
            _logger.LogWarning("This is a warning");
            _logger.LogTrace("This is a trace");
            _logger.LogInformation("This is informational");
            _logger.LogError("This is error");
            _logger.LogDebug("This is debug");
            _logger.LogCritical("This is critical");
            if (throwException) {
                throw new Exception("KABOOM!");
            }
            return "hello plain logs";
        }

        [Route("/[controller]/json")]
        [HttpGet]
        public ActionResult<string> JsonLogs(bool throwException=false) {
            _logger.LogWarning("This is a warning");
            _logger.LogTrace("This is a trace");
            _logger.LogInformation("This is informational");
            _logger.LogError("This is error");
            _logger.LogDebug("This is debug");
            _logger.LogCritical("This is critical");
            if (throwException) {
                throw new Exception("KABOOM!");
            }
            return "hello json logs";
        }
    }
}