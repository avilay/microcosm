using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WireCheckControllers {

    [DataContract]
    internal class LogEntry {
        [DataMember]
        internal string severity;

        [DataMember]
        internal string message;

        [DataMember]
        internal int errorCode;
    }

    // [Route("/[controller]")]
    [ApiController]
    public class WireCheckController : ControllerBase {
        private readonly ILogger<WireCheckController> _logger;

        public WireCheckController(ILogger<WireCheckController> logger) {
            _logger = logger;
        }

        private string ToJson(LogEntry log) {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(LogEntry));
            MemoryStream ms = new MemoryStream();
            ser.WriteObject(ms, log);
            ms.Position = 0;
            StreamReader reader = new StreamReader(ms);
            return reader.ReadToEnd();
        }

        [Route("/[controller]/plain")]
        [HttpGet]
        public ActionResult<string> PlainLogs(bool throwException = false) {
            _logger.LogDebug("This is debug");
            _logger.LogWarning("This is a warning");
            _logger.LogInformation("This is informational");
            _logger.LogError("This is error");
            _logger.LogCritical("This is critical");
            if (throwException) {
                throw new Exception("KABOOM!");
            }
            return "hello plain logs";
        }

        [Route("/[controller]/json")]
        [HttpGet]
        public ActionResult<string> JsonLogs(bool throwException = false) {
            _logger.LogDebug(ToJson(new LogEntry() {
                severity="DEBUG",
                message="This is debug",
                errorCode=4567
            }));
            _logger.LogWarning(ToJson(new LogEntry() {
                severity = "WARN",
                message = "This is a warning",
                errorCode = 1234
            }));
            _logger.LogInformation(ToJson(new LogEntry() {
                severity="INFO",
                message="This is informaitonal",
                errorCode=2345
            }));
            _logger.LogError(ToJson(new LogEntry() {
                severity="ERROR",
                message="This is an error",
                errorCode=3456
            }));
            _logger.LogCritical(ToJson(new LogEntry() {
                severity="CRITICAL",
                message="This is critical",
                errorCode=9873
            }));
            if (throwException) {
                throw new Exception("KABOOM!");
            }
            return "hello json logs";
        }
    }
}