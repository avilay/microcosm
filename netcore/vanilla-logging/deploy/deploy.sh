docker build -t vanilla-logging:$1 .
docker tag vanilla-logging:$1 gcr.io/unity-ml-avilay-test/vanilla-logging:$1
docker push gcr.io/unity-ml-avilay-test/vanilla-logging:$1
echo "Remeber to change deployment.yaml to deploy $1"
kubectl apply -f deploy/deployment.yaml
kubectl apply -f deploy/lb-service.yaml