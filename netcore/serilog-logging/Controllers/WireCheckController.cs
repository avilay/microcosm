using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WireCheckControllers {

    // [Route("/[controller]")]
    [ApiController]
    public class WireCheckController : ControllerBase {
        private readonly ILogger<WireCheckController> _logger;

        public WireCheckController(ILogger<WireCheckController> logger) {
            _logger = logger;
        }

        [Route("/[controller]")]
        [HttpGet]
        public ActionResult<string> PlainLogs(bool throwException = false) {
            _logger.LogDebug("{severity} This is debug {errorCode}", "DEBUG", 1234);
            _logger.LogWarning("{severity} This is a warning {errorCode}", "WARN", 2345);
            _logger.LogInformation("{severity} This is informational {errorCode}", "INFO", 4567);
            _logger.LogError("{severity} This is error {errorCode}", "ERROR", 8767);
            _logger.LogCritical("{severity} This is critical {errorCode}", "CRITICAL", 8762);
            if (throwException) {
                throw new Exception("KABOOM!");
            }
            return "hello logs";
        }
    }
}