docker build -t serilog-logging:$1 .
docker tag serilog-logging:$1 gcr.io/unity-ml-avilay-test/serilog-logging:$1
docker push gcr.io/unity-ml-avilay-test/serilog-logging:$1
echo "Remeber to change deployment.yaml to deploy $1"
kubectl apply -f deploy/deployment.yaml
kubectl apply -f deploy/lb-service.yaml
