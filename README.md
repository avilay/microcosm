# microcosm

To start a webapi in dev mode run it with the `ASPNETCORE_ENVIRONMENT=Development` environment variable set. This will use the appsetings.Development.json instead of appsettings.json.

A set of five microservices.

#### Frontend
This is the frontend API that takes in POST request and returns a response.

Request -
```
{
    query: "some search text here",
    range: {
        from: "2018-10-10",
        to: "2018-12-12"
    }
}
```

Response -
```
{
    id: 12345,
    consistent: True,
    rating: 3.45,
    quote: Some interesting quote,
    activites: [
        {timestamp: "2018-1-1", activity: "Tennis"},
        ...
    ]
}
```

It generates the ID itself and then sends the request to the second service.

#### ConsistencyService
Decides the value of the `consistency` field and then sends the request to the third and fourt services. They both return with one activity and rating and quote. The Evalutor then merges the response and sends it back.

#### ReviewService
Gets the rating for the response by generating the `rating` field and then sends the request to the fifth service.

#### QuoteService
Gets a random quote and then sends the request to the fifth service.

#### ActivityService
Generates an activity.