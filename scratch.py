import requests
import json

def main():
    body = {
        "range": {
            "from": "2018-10-10",
            "to": "2018-10-12"
        },
        "search": "Hello World"
    }
    resp = requests.post("http://localhost:5000/fast", json=body, verify=False)
    if resp.status_code == 200:
        print(resp.json())
    else:
        print(resp.status_code)

if __name__ == '__main__':
    main()
